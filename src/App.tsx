import React from 'react';
import Home from './Component/Home/Home';
import Login from './Component/Login/Login';
import Registration from './Component/Registration/Register';
import "./App.scss"


function App() {
  return (
    <div className="App">
     <Home/>
     <Login />
     <Registration/>
    </div>
  );
}

export default App;
