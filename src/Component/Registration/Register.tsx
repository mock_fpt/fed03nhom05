import React from 'react'
import "./Register.scss";

export default function Register() {
    return (
        <div className='Register'>
            <div className="model-login card m-auto">
                <div className="card-body">
                    <h4 className="text-center mb-4 mt-1">Registration</h4>
                    <form className="was-validated d-flex justify-content-between">
                        <div className="mb-3">
                            <input type="text" className="form-control is-valid" id="validationServer01" required placeholder='FirstName' />
                            <div className="valid-feedback">Looks good!</div>
                        </div>
                        <div className="mb-3">
                            <input type="text" className="form-control is-valid" required placeholder='LastName' />
                            <div className="valid-feedback">Looks good!</div>
                        </div>
                    </form>
                    <div className="mt-3 d-flex justify-content-between">
                        <div className="col-md-6">
                            <input type="text" className="form-control" id="inputCity" placeholder='Phone number' />
                        </div>
                        <div className="mb-3 was-validated">
                            <select className="form-select" required aria-label="select example">
                                <option value="">Open this select menu</option>
                                <option value="0" hidden>Male</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                            <div className="invalid-feedback">Example invalid select feedback</div>
                        </div>
                    </div>
                    <div className="mt-1">
                        <div className="mb-3">
                            <label className="form-label">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                            <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                        </div>
                    </div>
                    <div className="mt-5 d-flex justify-content-between">
                        <div className="row g-3 align-items-center">
                            <div className="col-auto">
                                <label className="col-form-label">Password:</label>
                            </div>
                            <div className="col-auto">
                                <input type="password" id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline" />
                            </div>
                            <div className="col-auto">
                                <span id="passwordHelpInline" className="form-text">
                                    Must be 8-20 characters long.
                                </span>
                            </div>
                        </div>
                        <div className="row g-3 align-items-center">
                            <div className="col-auto">
                                <label className="col-form-label">Confirm Password:</label>
                            </div>
                            <div className="col-auto">
                                <input type="password" id="inputPassword6" className="form-control" aria-describedby="passwordHelpInline" />
                            </div>
                            <div className="col-auto">
                                <span id="passwordHelpInline" className="form-text">
                                    Must be 8-20 characters long.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className='col-12 mt-4 d-flex '>
                    <div className="col-auto">
                        <label className="col-form-label">Please choose DD/MM/YYYY</label>     
                    </div>
                    <div className='ms-1 p-2'>
                        <input type="date" placeholder=''/>
                    </div>
                        
                    </div>
                    <div className="col-12 mt-5">
                        <div className="form-check">
                            <input className="form-check-input" type="checkbox" value="" id="invalidCheck" required />
                            <label className="form-check-label">Agree to terms and conditions</label>
                            <div className="invalid-feedback">You must agree before submitting.</div>
                        </div>
                    </div>
                    <div className="col-12 mt-4">
                        <button className="btn btn-primary" type="submit">Submit form</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
